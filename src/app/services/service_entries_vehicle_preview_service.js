angular.module('wheel').service('Entries', function (ref) {
  this.loadEntries = function ($scope, users) {
    ref.child('serviceEntries').orderByChild("car").equalTo($scope.vehicleId).once("value",
      function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          var entryKey = childSnapshot.key();
          var entryVal = childSnapshot.val();
          var mechanicId = entryVal.mechanic;

          var name = users.child(mechanicId).child('name');
          name.on("value", function (snapshot) {
            $scope.mechanicName = snapshot.val();
            $scope.entries.push({
              obj: entryVal,
              name: $scope.mechanicName,
              key: entryKey
            });
          });
        });
      });
  };
  this.openServiceEntryInModal = function($scope, obj) {
    $scope.showService = true;
    $scope.oneEntry = obj;
    $scope.entryDate = obj.obj.date;
    $scope.entryMechanist = obj.name;
    $scope.entryKm = obj.obj.distance;
    $scope.entryDesc = obj.obj.report;
    $scope.disableEntryDateChange = true;
    $scope.disableEntryMechanistChange = true;
    $scope.disableEntryDistanceChange = true;
    $scope.disableEntryReportChange = true;
  };
});
