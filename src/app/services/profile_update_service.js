angular.module('wheel').service('profileUpdate', function (ref, $log, $sessionStorage) {
  this.passwordUpdate = function (oldPass, newPass, userId) {
    userId.changePassword({
      email: ref.getAuth().password.email,
      oldPassword: oldOne,
      newPassword: newOne
    }, function (error) {
      if (error) {
        switch (error.code) {
          case "INVALID_PASSWORD":
            $log.log("Can't change email and password at the same time");
            break;
          case "INVALID_USER":
            alert('The specified user account does not exist.');
            $log.log("The specified user account does not exist.");
            break;
          default:
            alert('Error changing password: ' + error);
            $log.log("Error changing password:", error);
        }
      } else {
        alert('User password changed successfully!');
        $log.log("User password changed successfully!");
      }
    });
  };
  this.emailUpdate = function (newEmail, pass) {
    ref.changeEmail({
      oldEmail: ref.getAuth().password.email,
      newEmail: newEmail,
      password: pass
    }, function (error) {
      if (error) {
        switch (error.code) {
          case "INVALID_PASSWORD":
            alert('The specified user account password is incorrect. 1');
            $log.log("The specified user account password is incorrect.");
            break;
          case "INVALID_USER":
            alert('The specified user account does not exist.');
            $log.log("The specified user account does not exist.");
            break;
          default:
            alert('Error changing password: ' + error);
            $log.log("Error creating user:", error);
        }
      } else {
        alert('User email changed successfully!');
        $log.log("User email changed successfully!");
      }
    });
  };
  this.updateUserData = function (id, type, name, role, uName, userId, $scope) {
    if (uName !== '' && name !== '' && id !== '') {
      userId.set({
        id: id,
        legalPersonality: type,
        name: name,
        role: role,
        userName: uName,
        activated: true
      });
      $scope.userName = uName;
      $scope.userType = type;
      $scope.name = name;
      $scope.iAm = role;
      $scope.id = id;
      $sessionStorage.loggedInUser.userName = $scope.userName;
      $sessionStorage.loggedInUser.legalPersonality = $scope.userType;
      $sessionStorage.loggedInUser.name = $scope.name;
      $sessionStorage.loggedInUser.role = $scope.iAm;
      $sessionStorage.loggedInUser.id = $scope.id;
      alert('Profile updated successfully!');
    }
    else {
      alert('Please fill out all fields!');
    }
  };
  this.fillInputsOnProfile = function ($scope) {
    $scope.userName = $sessionStorage.loggedInUser.userName;
    $scope.userType = $sessionStorage.loggedInUser.legalPersonality;
    $scope.name = $sessionStorage.loggedInUser.name;
    $scope.iAm = $sessionStorage.loggedInUser.role;
    $scope.id = $sessionStorage.loggedInUser.id;
  };
});
