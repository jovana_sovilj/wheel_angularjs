angular.module('wheel').service('EmailService', function (ref, $log, HeaderManipulation) {
  this.initializeScope = function($scope) {
    $scope.messages = [];
    $scope.tabs = 'inbox';
    $scope.showMessageInModal = false;
    $scope.sendNewMessageModal = false;
  };
  this.ifTab = function(obj, $scope) {
    if($scope.tabs == 'inbox' || $scope.tabs == 'sent') {
      $scope.openMessage(obj);
    }
    else if($scope.tabs == 'drafts') {
      $scope.sendNewMessage(obj);
    }
  };
  this.showMessage = function(obj, $scope, users) {
    $scope.showMessageInModal = true;
    var messageId = obj.key;
    var messageRef = new Firebase("https://wheelapp.firebaseio.com/messages/" + messageId);
    messageRef.child('read').once("value", function (snapshot) {
      if (snapshot.val() == false) {
        messageRef.child('read').set(true);
        HeaderManipulation.showUnreadMessages($scope, users);
        obj.obj.read = true;
        if(!$scope.$$phase) {
          $scope.$apply();
        }
      }
    });
    if($scope.tabs == 'inbox') {
      $scope.messageTo = 'Me';
    }
    else {
      $scope.messageTo = obj.obj.to;
    }
    $scope.messageDate = obj.obj.date;
    $scope.messageTitle = obj.obj.subject;
    $scope.messageBody = obj.obj.body;
    if($scope.tabs == 'sent' || $scope.tabs == 'drafts') {
      $scope.messageFrom = 'Me';
    }
    else {
      $scope.messageFrom = obj.name;
    }
  };
  this.loadAllMessagesByTabs = function($scope, users, messages) {
    $scope.messages.splice(0);
    var name = users.child(ref.getAuth().uid).child('name');
    if ($scope.tabs == 'inbox') {
      name.on("value", function (snapshot) {
        var usersName = snapshot.val();
        messages.once("value", function (snapshot) {
          $scope.countInbox = 0;
          snapshot.forEach(function (childSnapshot) {
            var messageKey = childSnapshot.key();
            var messageObj = childSnapshot.val();
            var to = messageObj.to;
            var from = messageObj.from;
            var fromName = users.child(from).child('name');
            fromName.on("value", function (snapshot) {
              var name = snapshot.val();
              if (to == usersName) {
                $scope.messages.push({obj: messageObj, name: name, key: messageKey});
                $scope.countInbox++;
                $scope.$apply();
              }
            });
          });
        });
      });
    }
    else if ($scope.tabs == 'sent') {
      messages.once("value", function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          var messageKey = childSnapshot.key();
          var messageObj = childSnapshot.val();
          var to = messageObj.to;
          var from = messageObj.from;
          if (from == ref.getAuth().uid) {
            $scope.messages.push({obj: messageObj, name: to, key: messageKey});
            $scope.$apply();
          }
        });
      });
    }
    else if($scope.tabs == 'drafts') {
      var myId = ref.getAuth().uid;
      ref.child('messageDrafts').orderByChild("from").equalTo(myId).once("value",
        function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            var draftKey = childSnapshot.key();
            var draft = childSnapshot.val();
            var name = draft.to;
            $scope.messages.push({obj: draft, name: name, key: draftKey});
            $scope.$apply();
          });
        });
    }
  };
  this.openModalToSendMessage = function(obj, $scope) {
    $scope.sendNewMessageModal = true;
    $scope.newMessageTo = obj.name;
    $scope.messageKey = obj.key;
    var newMessageTitle;
    if($scope.tabs == 'drafts') {
      $scope.newMessageBodyText = obj.obj.body;
    }
    else {
      $scope.newMessageBodyText = '';
    }
    var subject = obj.obj.subject;
    if (subject.search('RE:') != -1) {
      newMessageTitle = obj.obj.subject;
      $scope.messageSubject = newMessageTitle;
    }
    else {
      newMessageTitle = 'RE:' + obj.obj.subject;
      $scope.messageSubject = newMessageTitle;
    }
  };
  this.saveNewDraft = function($scope, messageDrafts) {
    var messageBody = $scope.newMessageBodyText;
    var name = $scope.newMessageTo;
    var title = $scope.messageSubject;
    var date = '--.--.--.';
    var newDraftRef = messageDrafts.push();
    newDraftRef.set({
      from: ref.getAuth().uid,
      to: name,
      subject: title,
      body: messageBody,
      date: date
    });
    alert('Successfully created a new draft! Checkout all your draft' +
      ' messages in the drafts tab(next to tab sent).');
  };
  this.deleteSpecificDraft = function($scope, key) {
    var draftRef = new Firebase("https://wheelapp.firebaseio.com/messageDrafts/" + key);
    draftRef.remove();
    $scope.closeMessage();
  };
  this.sendNewMessageToOwner = function($scope, messages) {
    var messageBody = $scope.newMessageBodyText;
    var name = $scope.newMessageTo;
    var title = $scope.messageSubject;
    var date = this.getMessageDate();
    var time = this.getMessageTime();
    var read = false;
    if (messageBody != '') {
      var newMessageRef = messages.push();
      newMessageRef.set({
        from: ref.getAuth().uid,
        to: name,
        read: read,
        subject: title,
        body: messageBody,
        date: date,
        time: time
      });
      $scope.closeMessage();
    }
    else {
      alert('Please fill out the message body field!');
    }
  };
  this.getMessageDate = function() {
    var fullDate = new Date();
    var year = fullDate.getFullYear();
    var month = fullDate.getMonth() + 1;
    var day = fullDate.getDate();
    var date = day + '.' + month + '.' + year + '.';
    return date;
  };
  this.getMessageTime = function() {
    var fullDate = new Date();
    var h = fullDate.getHours();
    var m = fullDate.getMinutes();
    var time = h + ':' + m;
    return time;
  };


});
