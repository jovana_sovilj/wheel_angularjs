angular.module('wheel').service('ForSaleAndOwner', function (filterVehicles, ref) {
  this.initializeScope = function ($scope) {
    $scope.carsForSaleObjArr = [];
    $scope.vehicleObjArr = [];
    $scope.years = [];
    $scope.brands = [];
    $scope.sortByOptions = [];
    $scope.edmundsModels = [];
    $scope.tabs = 'all';
    $scope.brand = 'All';
    $scope.addNewVBrand = 'All';
    $scope.yearFrom = '1980';
    $scope.kms = [0, 10000, 50000, 100000, 150000, 200000, 1000000];
    $scope.kmFrom = '0';
    $scope.kmTo = '1000000';
    $scope.sortByOptions = ['Date of last repair', 'Year(inc)', 'Year(dec)', 'Km(inc)', 'Km(dec)'];
    $scope.sortBy = 'Date of last repair';
    $scope.sendNewMessageModal = false;
    $scope.showModalSell = false;
    $scope.showModalForAdding = false;
  };
  this.filterVehiclesForSale = function ($scope) {
    var sortOption = $scope.sortBy;
    ref.child('vehicles').orderByChild("forSale").equalTo('yes').once("value",
      function (snapshot) {
        $scope.ref = ref.getAuth().uid;
        $scope.numberOfCars = 0;
        $scope.carsForSaleObjArr.splice(0);
        snapshot.forEach(function (childSnapshot) {
          var childData = childSnapshot.val();
          var yearFrom = parseInt($scope.yearFrom);
          var yearTo = parseInt($scope.yearTo);
          var brand = $scope.brand;
          var kmFrom = parseInt($scope.kmFrom);
          var kmTo = parseInt($scope.kmTo);
          var search = $scope.searchVehicles;
          $scope.ownerId = childData.ownerId;
          var filters = [];
          filters.push(yearFrom, yearTo, brand, kmFrom, kmTo);
          var filterResults = filterVehicles.applyFilters(childData, filters, search);
          if (filterResults) {
            $scope.carsForSaleObjArr.push(childData);
            $scope.numberOfCars++;
            filterVehicles.sortVehicles($scope.carsForSaleObjArr, sortOption);
          }
          $scope.$apply();
        });
      });
  };
  this.filterVehiclesOnOwnerIndex = function ($scope, vehicles) {
    var sortOption = $scope.sortBy;
    vehicles.once("value", function (snapshot) {
      $scope.ref = ref.getAuth().uid;
      var filterResults;
      $scope.vehicleObjArr.splice(0);
      snapshot.forEach(function (childSnapshot) {
        var childData = childSnapshot.val();
        var yearFrom = parseInt($scope.yearFrom);
        var yearTo = parseInt($scope.yearTo);
        var brand = $scope.brand;
        var kmFrom = parseInt($scope.kmFrom);
        var kmTo = parseInt($scope.kmTo);
        var search = $scope.searchVehicles;
        var filters = [];
        filters.push(yearFrom, yearTo, brand, kmFrom, kmTo);
        if (childData.ownerId == $scope.ref) {
          if ($scope.tabs == 'all') {
            filterResults = filterVehicles.applyFilters(childData, filters, search);
            if (filterResults) {
              $scope.vehicleObjArr.push(childData);
              filterVehicles.sortVehicles($scope.vehicleObjArr, sortOption);
            }
            $scope.$apply();
          }
          else if ($scope.tabs == 'for sale') {
            if (childData.forSale == 'yes') {
              filterResults = filterVehicles.applyFilters(childData, filters, search);
              if (filterResults) {
                $scope.vehicleObjArr.push(childData);
                filterVehicles.sortVehicles($scope.vehicleObjArr, sortOption);
              }
              $scope.$apply();
            }
          }
          else if ($scope.tabs == 'on rep') {
            if (childData.onRep == 'yes') {
              filterResults = filterVehicles.applyFilters(childData, filters, search);
              if (filterResults) {
                $scope.vehicleObjArr.push(childData);
                filterVehicles.sortVehicles($scope.vehicleObjArr, sortOption);
              }
              $scope.$apply();
            }
          }
        }
      });
    });
  };
  this.edmundsModels = function ($scope) {
    var selectedBrand = $scope.addNewVBrand;
    if (selectedBrand != '') {
      $.getJSON("https://api.edmunds.com/api/vehicle/v2/" + selectedBrand + "/models?fmt=json&api_key=qhf7nezafzvdbbzum58558qm", function (data) {
        $scope.edmundsModels.splice(0);
        for (var i = 0; i < data.modelsCount; i++) {
          $scope.edmundsModels.push(data.models[i].name);
        }
        if (!$scope.$$phase) {
          $scope.$apply();
        }
      });
    } else {
      $.getJSON("https://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key=qhf7nezafzvdbbzum58558qm", function (data) {
        $.getJSON("https://api.edmunds.com/api/vehicle/v2/" + data.makes[1].name + "/models?fmt=json&api_key=qhf7nezafzvdbbzum58558qm", function (data) {
          $scope.edmundsModels.splice(0);
          for (var i = 0; i < data.modelsCount; i++) {
            $scope.edmundsModels.push(data.models[i].name);
          }
          if (!$scope.$$phase) {
            $scope.$apply();
          }
        });
      });
    }
  };
  this.addNewVehicle = function ($scope) {
    var brand = $scope.addNewVBrand;
    var model = $scope.addNewVModel;
    var year = $scope.addNewVYear;
    var km = $scope.addNewVKm;
    var plate = $scope.addNewVPlate;
    var id = $scope.addNewVId;
    var ccm = $scope.addNewVCcm;
    var regU = $scope.addNewVRegUntil;
    var regUntil = this.fixDate(regU);
    var forSale = $scope.addNewVForSale;
    var onRep = $scope.addNewVOnRep;
    var image = 'https://www.amwayhappynings.com/uploads/blog/no-image-yet.jpg';
    if (year != '' && km != '' && plate != '' && id != '' && ccm != '' && regUntil != '') {
      ref.child('vehicles').orderByChild("id").equalTo(id).once("value",
        function (snapshot) {
          var vehicleVal = snapshot.val();
          if (vehicleVal != null) {
            alert('Vehicle with this ID' +
              ' already exists!');
          }
          else {
            var vehicleKey = id;
            var vehicleObj = new Firebase("https://wheelapp.firebaseio.com/vehicles/" + vehicleKey);
            vehicleObj.set({
              brand: brand,
              model: model,
              year: year,
              km: km,
              plate: plate,
              id: id,
              ccm: ccm,
              regUntil: regUntil,
              forSale: forSale,
              onRep: onRep,
              ownerId: $scope.ref,
              lastRepair: 'unknown',
              image: image
            });
            $scope.showAddNewVehicleModal();
            $scope.$apply();
          }
        });
    }
    else {
      alert('Please fill out all the fields!');
    }
  };
  this.fixDate = function (input) {
    var year = input.getFullYear();
    var month = input.getMonth() + 1;
    var day = input.getDate();
    var newDay;
    var newMonth;
    if (day < 10) {
      newDay = '0' + day;
    }
    else {
      newDay = day;
    }
    if (month < 10) {
      newMonth = '0' + month;
    }
    else {
      newMonth = month;
    }
    var date = year + '-' + newMonth + '-' + newDay;
    return date;
  };
  this.sendNotificationToNewOwner = function($scope, users, notifications) {
    var vehicleId = $scope.vehicleId;
    var sellCarToId = $scope.userId;
    var name = users.child(ref.getAuth().uid).child('name');
    name.on("value", function (snapshot) {
      var myName = snapshot.val();
      ref.child('vehicles').orderByChild("id").equalTo(vehicleId).once("value",
        function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            var vehicle = childSnapshot.val();
            console.log(vehicle);
            var brand = vehicle.brand;
            var model = vehicle.model;
            var year = vehicle.year;
            var plate = vehicle.plate;
            var body = myName + ' offered you ' + brand + ' ' + model + ' ' + year + ' (' + plate + ').';
            var state = false;
            var type = 'sell';
            var from = ref.getAuth().uid;
            ref.child('users').orderByChild("id").equalTo(sellCarToId).once("value",
              function (snapshot) {
                var userVal = snapshot.val();
                console.log(userVal);
                if (userVal == null || userVal == ref.getAuth().uid) {
                  alert('Invalid User ID!');
                }
                else {
                  snapshot.forEach(function (childSnapshot) {
                    var key = childSnapshot.key();
                    if (key != ref.getAuth().uid) {
                      var newNotificationRef = notifications.push();
                      newNotificationRef.set({
                        from: from,
                        to: key,
                        type: type,
                        body: body,
                        state: state,
                        car: vehicleId
                      });
                      $scope.closeModalSell();
                      if (!$scope.$$phase) {
                        $scope.$apply();
                      }
                    }
                  });
                }
              });
          });
        });
    });
  };

});
