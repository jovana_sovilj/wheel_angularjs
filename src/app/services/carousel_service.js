angular.module('wheel').service('Carousel', function (ref) {
  this.showMainPhoto = function ($scope) {
    var vehicleImage = ref.child('vehicles').child($scope.vehicleId).child('image');
    vehicleImage.once("value", function (snap) {
      $scope.imageSrc = snap.val();
    });
  };
  this.showOtherPhotos = function ($scope) {
    $scope.countImages = 1;
    $scope.images = [];
    var imageUrls = ref.child('vehicles').child($scope.vehicleId).child('images');
    imageUrls.once("value", function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        var childDataImage = childSnapshot.val();
        $scope.images.push(childDataImage);
        $scope.countImages++;
      });
    });
  };
  this.showPhotosInCarousel = function ($scope, slides) {
    var currIndex = 0;
    slides.splice(0);
    var vehicle = ref.child('vehicles').child($scope.vehicleId).child('image');
    vehicle.once("value", function (snap) {
      slides.push({
        image: snap.val(),
        id: currIndex++,
        key: snap.key()
      });
    });
    var imageUrls = ref.child('vehicles').child($scope.vehicleId).child('images');
    imageUrls.once("value", function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        var childData = childSnapshot.val();
        slides.push({
          image: childData,
          id: currIndex++,
          key: childSnapshot.key()
        });
      });
    });
  };
  this.pictures = function ($scope) {
    var self = this;
    $scope.myInterval = 5000;
    $scope.noWrapSlides = false;
    $scope.active = 0;
    var slides = $scope.slides = [];
    $scope.show = false;
    function LoadPhotos() {
      self.showPhotosInCarousel($scope, slides);
    }
    LoadPhotos();
    $scope.showPhotoModal = function () {
      $scope.show = true;
      $scope.photoUrl = '';
    };
    $scope.closePhotoModal = function () {
      $scope.show = false;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.uploadPhoto = function () {
      var imageUrl = $scope.photoUrl.trim();
      testImage(imageUrl, record);
    };
    function testImage(url, callback, timeout) {
      timeout = timeout || 5000;
      var timedOut = false, timer;
      var img = new Image();
      img.onerror = img.onabort = function () {
        if (!timedOut) {
          clearTimeout(timer);
          callback(url, "error");
        }
      };
      img.onload = function () {
        if (!timedOut) {
          clearTimeout(timer);
          callback(url, "success");
        }
      };
      img.src = url;
      timer = setTimeout(function () {
        timedOut = true;
        callback(url, "timeout");
      }, timeout);
    }
    function record(url, result) {
      if (result == 'success') {
        var main = ref.child('vehicles').child($scope.vehicleId).child('image');
        main.once("value", function (snap) {
          var urlMain = snap.val();
          if (urlMain == 'https://www.amwayhappynings.com/uploads/blog/no-image-yet.jpg') {
            ref.child('vehicles').child($scope.vehicleId).child('image').set(url);
            $scope.showPhotoModal();
            LoadPhotos();
          }
          else {
            ref.child('vehicles').child($scope.vehicleId).child('images').push(url);
            $scope.showPhotoModal();
            LoadPhotos();
          }
        });
      }
      else {
        alert('You have entered wrong URL. Try again.');
      }
    }
    $scope.selectAsMain = function (key) {
      var keyCandidate = key;
      var photoCandidate = ref.child('vehicles').child($scope.vehicleId).child('images').child(keyCandidate);
      photoCandidate.once("value", function (snap) {
        var urlCandidate = snap.val();
        var main = ref.child('vehicles').child($scope.vehicleId).child('image');
        main.once("value", function (snap) {
          var urlMain = snap.val();
          ref.child('vehicles').child($scope.vehicleId).child('image').set(urlCandidate);
          ref.child('vehicles').child($scope.vehicleId).child('images').child(keyCandidate).set(urlMain);
          LoadPhotos();
        });
      });
    };
    $scope.deletePhoto = function (key) {
      var photoKey = key;
      ref.child('vehicles').child($scope.vehicleId).child('images').child(photoKey).remove();
      LoadPhotos();
    };
  };
});
