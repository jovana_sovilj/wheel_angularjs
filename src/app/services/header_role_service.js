angular.module('wheel').service('HeaderManipulation', function (ref, $log, $sessionStorage) {
  this.loadDifferentHeader = function ($scope, users) {
    if (ref.getAuth() === null || ref.getAuth() === undefined) {
      $scope.role = 'Guest';
      $log.log($scope.role);
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    }
    else if (ref.getAuth() !== null || ref.getAuth() !== undefined) {
      var getRole = users.child(ref.getAuth().uid).child('role');
      getRole.once("value", function (snapshot) {
        $scope.logged = snapshot.val();
        if ($scope.logged === 'Mechanic') {
          $scope.role = 'Mechanic';
          $log.log($scope.role);
          if (!$scope.$$phase) {
            $scope.$apply();
          }
        }
        else if ($scope.logged === 'Vehicle owner') {
          $scope.role = 'Vehicle owner';
          $log.log($scope.role);
          if (!$scope.$$phase) {
            $scope.$apply();
          }
        }
        else if ($scope.logged === 'admin') {
          $scope.role = 'admin';
          $log.log($scope.role);
          if (!$scope.$$phase) {
            $scope.$apply();
          }
        }
      });
    }
  };
  this.showUnreadMessages = function ($scope, users) {
    if (ref.getAuth() != null) {
      var name = users.child(ref.getAuth().uid).child('name');
      name.on("value", function (snapshot) {
        var usersName = snapshot.val();
        ref.child('messages').orderByChild("to").equalTo(usersName).once("value",
          function (snapshot) {
            var count = 0;
            snapshot.forEach(function (childSnapshot) {
              var myMessages = childSnapshot.val();
              var state = myMessages.read;
              if (state == false) {
                count++;
              }
            });
            $scope.numberOfUnreadMessages = count;
            $scope.$apply();
          });
      });
    }
  };
  this.showUnseenNotifications = function ($scope) {
    if (ref.getAuth() != null) {
      var key = ref.getAuth().uid;
      ref.child('notifications').orderByChild("to").equalTo(key).once("value",
        function (snapshot) {
          var count = 0;
          $scope.notificationsArr.splice(0);
          snapshot.forEach(function (childSnapshot) {
            var keyNotification = childSnapshot.key();
            var myNotifications = childSnapshot.val();
            var state = myNotifications.state;
            if (state == false) {
              count++;
              $scope.notificationsArr.push({
                key: keyNotification,
                obj: myNotifications
              });
            }
          });
          $scope.numberOfUnseenNotifications = count;
          $scope.$apply();
        });
    }
  };
  this.declineSellInNotification = function (key, $scope, users, notifications) {
    var self = this;
    var notificationId = key;
    var notification = notifications.child(notificationId);
    var name = users.child(ref.getAuth().uid).child('name');
    name.on("value", function (snapshot) {
      var myName = snapshot.val();
      notification.once("value", function (snapshot) {
        var declinedNotification = snapshot.val();
        var to = declinedNotification.from;
        var from = declinedNotification.to;
        var carId = declinedNotification.car;
        ref.child('vehicles').orderByChild("id").equalTo(carId).once("value", function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            var vehicle = childSnapshot.val();
            var brand = vehicle.brand;
            var model = vehicle.model;
            var body = myName + " has declined your offer for " + brand + " " + model + "!";
            var state = false;
            var type = 'response-sell';
            var newNotificationRef = notifications.push();
            newNotificationRef.set({
              from: from,
              to: to,
              type: type,
              body: body,
              state: state,
              car: carId
            });
          });
        });
      });
    });
    notification.child('state').once("value", function (snapshot) {
      if (snapshot.val() == false) {
        notification.child('state').set('declined');
        self.showUnseenNotifications($scope);
        if (!$scope.$$phase) {
          $scope.$apply();
        }
      }
    });
  };
  this.okResponseOnNotification = function (key, $scope, notifications) {
    var self = this;
    var notificationId = key;
    var notification = notifications.child(notificationId);
    notification.child('state').once("value", function (snapshot) {
      if (snapshot.val() == false) {
        notification.child('state').set(true);
        self.showUnseenNotifications($scope);
        if (!$scope.$$phase) {
          $scope.$apply();
        }
      }
    });
  };
  this.acceptSellInNotification = function (key, $scope, notifications, users, vehicles) {
    var self = this;
    var notificationId = key;
    var notification = notifications.child(notificationId);
    var name = users.child(ref.getAuth().uid).child('name');
    name.on("value", function (snapshot) {
      var myName = snapshot.val();
      notification.once("value", function (snapshot) {
        var declinedNotification = snapshot.val();
        var to = declinedNotification.from;
        var from = declinedNotification.to;
        var carId = declinedNotification.car;
        ref.child('vehicles').orderByChild("id").equalTo(carId).once("value", function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            var vehicle = childSnapshot.val();
            var brand = vehicle.brand;
            var model = vehicle.model;
            var body = myName + " has accepted your offer for " + brand + " " + model + "!";
            var state = false;
            var type = 'response-sell';
            var newNotificationRef = notifications.push();
            newNotificationRef.set({
              from: from,
              to: to,
              type: type,
              body: body,
              state: state,
              car: carId
            });
            var carToUpdate = vehicles.child(carId);
            carToUpdate.child('ownerId').set(from);
          });
        });
      });
    });
    notification.child('state').once("value", function (snapshot) {
      if (snapshot.val() == false) {
        notification.child('state').set('accepted');
        self.showUnseenNotifications($scope);
        if (!$scope.$$phase) {
          $scope.$apply();
        }
      }
    });
  };
  this.GetToday = function () {
    var fullDate = new Date();
    var year = fullDate.getFullYear();
    var month = fullDate.getMonth() + 1;
    var day = fullDate.getDate();
    var newMonth;
    var newDay;
    if (day < 10) {
      newDay = '0' + day;
    }
    else {
      newDay = day;
    }
    if (month < 10) {
      newMonth = '0' + month;
    }
    else {
      newMonth = month;
    }
    var date = year + '-' + newMonth + '-' + newDay;
    return date;
  };
  this.registration = function ($scope, notifications) {
    var self = this;
    if (ref.getAuth() != null) {
      ref.child('vehicles').orderByChild("ownerId").equalTo(ref.getAuth().uid).once("value",
        function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            var childData = childSnapshot.val();
            var regUntil = childData.regUntil;
            var ownId = childData.ownerId;
            var carId = childData.id;
            var todayDate = self.GetToday();
            if (regUntil == todayDate) {
              if ($sessionStorage.registrationFlag == false) {
                var body = 'We just want to remind you that your registration' +
                  ' for ' + childData.brand + ' ' + childData.model + ' has expired today' +
                  ' and that you' +
                  ' should' +
                  ' renew it.';
                var state = false;
                var type = 'registration-reminder';
                var newNotificationRef = notifications.push();
                newNotificationRef.set({
                  from: ownId,
                  to: ownId,
                  type: type,
                  body: body,
                  state: state,
                  car: carId,
                  date: todayDate
                });
              }
            }
          });
          $sessionStorage.registrationFlag = true;
          $scope.$apply();
        });
    }
  };
  this.service = function ($scope, notifications) {
    var self = this;
    if (ref.getAuth() != null) {
      ref.child('vehicles').orderByChild("ownerId").equalTo(ref.getAuth().uid).once("value",
        function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            var childData = childSnapshot.val();
            var lastRepairFromFirebase = childData.lastRepair;
            var lastRepairArr = self.splitDate(lastRepairFromFirebase);
            var yearOfLastRepair = parseInt(lastRepairArr[2]);
            var yearOfNewRepair = (yearOfLastRepair).toString();
            lastRepairArr[2] = yearOfNewRepair;
            var lastRepair = lastRepairArr.join('.');
            var ownId = childData.ownerId;
            var carId = childData.id;
            var todayDate = self.GetTodayForService();
            if (lastRepair == todayDate) {
              if ($sessionStorage.serviceFlag == false) {
                var body = 'We just want to remind you that your last' +
                  ' service of ' + childData.brand + ' ' + childData.model + ' was a year ago. You should consider taking your vehicle for the annual maintenance to your mechanic.';
                var state = false;
                var type = 'service-reminder';
                var newNotificationRef = notifications.push();
                newNotificationRef.set({
                  from: ownId,
                  to: ownId,
                  type: type,
                  body: body,
                  state: state,
                  car: carId,
                  date: todayDate
                });
              }
            }
          });
          $sessionStorage.serviceFlag = true;
          $scope.$apply();
        });
    }
  };
  this.splitDate = function (date) {
    var splitDate = date;
    var result = splitDate.split('.');
    return result;
  };
  this.GetTodayForService = function () {
    var fullDate = new Date();
    var year = fullDate.getFullYear();
    var month = fullDate.getMonth() + 1;
    var day = fullDate.getDate();
    var newMonth;
    var newDay;
    if (day < 10) {
      newDay = '0' + day;
    }
    else {
      newDay = day;
    }
    if (month < 10) {
      newMonth = '0' + month;
    }
    else {
      newMonth = month;
    }
    var date = newDay + '.' + newMonth + '.' + year + '.';
    return date;
  };


});
