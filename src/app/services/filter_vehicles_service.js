angular.module('wheel').factory('filterVehicles', function () {
  return {
    applyFilters: function (vehicleObj, filters, search) {
      if (search == undefined || search == null) {
        if (((filters[0] <= vehicleObj.year) && (vehicleObj.year <= filters[1]))
          && (filters[2] == vehicleObj.brand || filters[2] == 'All') && (filters[3] <= vehicleObj.km) && (vehicleObj.km <= filters[4])) {
          return true;
        }
        else {
          return false;
        }
      }
      else {
        var querywords = search.split(' ');
        if (((filters[0] <= vehicleObj.year) && (vehicleObj.year <= filters[1]))
          && (filters[2] == vehicleObj.brand || filters[2] == 'All') && (filters[3] <= vehicleObj.km) && (vehicleObj.km <= filters[4])) {
          for (var i = 0; i < querywords.length; i++) {
            if (vehicleObj.brand.toUpperCase().search(querywords[i].toUpperCase()) != -1 ||
              vehicleObj.model.toUpperCase().search(querywords[i].toUpperCase()) != -1 ||
              vehicleObj.year.toUpperCase().search(querywords[i].toUpperCase()) != -1 ||
              vehicleObj.plate.toUpperCase().search(querywords[i].toUpperCase()) != -1) {
              return true;
            }
          }
        }
        else {
          return false;
        }
      }
    },
    sortVehicles: function (vehicleObjArr, sortBy) {
      if (sortBy === 'Date of last repair') {
        vehicleObjArr.sort(SortByLastRepair);
        return vehicleObjArr;
      }
      function SortByLastRepair(a, b) {
        var dateA = (a.lastRepair[6] + a.lastRepair[7] + a.lastRepair[8] + a.lastRepair[9] + a.lastRepair[3] + a.lastRepair[4] + a.lastRepair[0] + a.lastRepair[1]);
        var dateB = (b.lastRepair[6] + b.lastRepair[7] + b.lastRepair[8] + b.lastRepair[9] + b.lastRepair[3] + b.lastRepair[4] + b.lastRepair[0] + b.lastRepair[1]);
        if (dateA < dateB) {
          return -1;
        }
        else if (dateA > dateB) {
          return 1;
        }
        else {
          return 0;
        }
      }
      if (sortBy === 'Year(inc)') {
        vehicleObjArr.sort(SortByYearInc);
        return vehicleObjArr;
      }
      if (sortBy === 'Year(dec)') {
        vehicleObjArr.sort(SortByYearInc);
        vehicleObjArr.reverse();
        return vehicleObjArr;
      }
      function SortByYearInc(a, b) {
        var yearA = parseInt(a.year);
        var yearB = parseInt(b.year);

        if (yearA < yearB) {
          return -1;
        }
        else if (yearA > yearB) {
          return 1;
        }
        else {
          return 0;
        }
      }
      if (sortBy == 'Km(inc)') {
        vehicleObjArr.sort(SortByKmInc);
        return vehicleObjArr;
      }
      if (sortBy == 'Km(dec)') {
        vehicleObjArr.sort(SortByKmInc);
        vehicleObjArr.reverse();
        return vehicleObjArr;
      }
      function SortByKmInc(a, b) {
        var kmA = parseInt(a.km);
        var kmB = parseInt(b.km);

        if (kmA < kmB) {
          return -1;
        }
        else if (kmA > kmB) {
          return 1;
        }
        else {
          return 0;
        }
      }
    }

  };
});
