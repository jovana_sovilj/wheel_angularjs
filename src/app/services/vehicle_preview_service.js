angular.module('wheel').service('VehiclePreview', function (ref, $sessionStorage) {
  this.initializeScope = function($scope) {
    $scope.vehicleId = $sessionStorage.vehicleId;
    $scope.edmundsArr = [];
    $scope.edmundsModels = [];
    $sessionStorage.edmundsArr = [];
    $scope.entries = [];
    $scope.showForSaleLabel = false;
    $scope.showOnRepLabel = false;
    $scope.showService = false;
  };
  this.edmundsBrands = function ($scope) {
    $.getJSON("https://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key=qhf7nezafzvdbbzum58558qm", function (data) {
      for (var i = 0; i < data.makesCount - 1; i++) {
        $scope.edmundsArr.push(data.makes[i].name);
        $sessionStorage.edmundsArr.push(data.makes[i].name);
      }
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    });
  };
  this.edmundsModels = function ($scope) {
    var selectedBrand = $scope.brand;
    if (selectedBrand != '') {
      $.getJSON("https://api.edmunds.com/api/vehicle/v2/" + selectedBrand + "/models?fmt=json&api_key=qhf7nezafzvdbbzum58558qm", function (data) {
        $scope.edmundsModels.splice(0);
        for (var i = 0; i < data.modelsCount; i++) {
          $scope.edmundsModels.push(data.models[i].name);
        }
        if (!$scope.$$phase) {
          $scope.$apply();
        }
      });
    } else {
      $.getJSON("https://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key=qhf7nezafzvdbbzum58558qm", function (data) {
        $.getJSON("https://api.edmunds.com/api/vehicle/v2/" + data.makes[1].name + "/models?fmt=json&api_key=qhf7nezafzvdbbzum58558qm", function (data) {
          $scope.edmundsModels.splice(0);
          for (var i = 0; i < data.modelsCount; i++) {
            $scope.edmundsModels.push(data.models[i].name);
          }
          if (!$scope.$$phase) {
            $scope.$apply();
          }
        });
      });
    }
  };
  this.vehicleDetails = function ($scope, users) {
    var self = this;
    ref.child('vehicles').orderByChild("id").equalTo($scope.vehicleId).once("value",
      function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          var childData = childSnapshot.val();
          $scope.brand = childData.brand;
          $scope.selectModelFromEdmunds();
          $scope.model = childData.model;
          $scope.year = childData.year;
          var ownersId = users.child(childData.ownerId);
          ownersId.on("value", function (snapshot) {
            var user = snapshot.val();
            $scope.owner = user.userName;
          });
          $scope.plate = childData.plate;
          $scope.id = childData.id;
          $scope.regUntil = childData.regUntil;
          $scope.ccm = childData.ccm;
          $scope.km = childData.km;
          $scope.forSale = childData.forSale;
          $scope.onRep = childData.onRep;
          self.forSaleLabel($scope.forSale, $scope);
          self.onRepLabel($scope.onRep, $scope);
        });
      });
  };
  this.forSaleLabel = function (sale, $scope) {
    if (sale == 'yes') {
      $scope.showForSaleLabel = true;
    }
    else {
      $scope.showForSaleLabel = false;
    }
  };
  this.onRepLabel = function (rep, $scope) {
    if (rep == 'yes') {
      $scope.showOnRepLabel = true;
    }
    else {
      $scope.showOnRepLabel = false;
    }
  };
  this.vehicleUpdate = function ($scope, vehicles) {
    var currentVehicle = vehicles.child($scope.vehicleId);
    var self = this;
    if ($scope.brand != null || $scope.brand != undefined) {
      currentVehicle.child('brand').set($scope.brand);
      $scope.disableBrandChange = true;
    }
    if ($scope.model != null || $scope.model != undefined) {
      currentVehicle.child('model').set($scope.model);
      $scope.disableModelChange = true;
    }
    if (($scope.year != null || $scope.year != undefined) && $scope.year != '') {
      currentVehicle.child('year').set($scope.year);
      $scope.disableYearChange = true;
    }
    if (($scope.plate != null || $scope.plate != undefined) && $scope.plate != '') {
      currentVehicle.child('plate').set($scope.plate);
      $scope.disablePlateChange = true;
    }
    if (($scope.regUntil != null || $scope.regUntil != undefined) && $scope.regUntil != '') {
      currentVehicle.child('regUntil').set($scope.regUntil);
      $scope.disableRegUntilChange = true;
    }
    if (($scope.ccm != null || $scope.ccm != undefined) && $scope.ccm != '') {
      currentVehicle.child('ccm').set($scope.ccm);
      $scope.disableCcmChange = true;
    }
    if (($scope.km != null || $scope.km != undefined) && $scope.km != '') {
      currentVehicle.child('km').set($scope.km);
      $scope.disableKmChange = true;
    }
    if ($scope.disableForSaleChange == false) {
      if ($scope.forSale != null || $scope.forSale != undefined) {
        currentVehicle.child('forSale').set($scope.forSale);
        self.forSaleLabel($scope.forSale, $scope);
        $scope.disableForSaleChange = true;
      }
    }
    if ($scope.disableOnRepChange == false) {
      if ($scope.onRep != null || $scope.onRep != undefined) {
        currentVehicle.child('onRep').set($scope.onRep);
        self.onRepLabel($scope.onRep, $scope);
        $scope.disableOnRepChange = true;
      }
    }
    if ($scope.year == '' || $scope.plate == '' || $scope.regUntil == '' || $scope.ccm == '' || $scope.km == '') {
      alert('Please fill out all fields!');
    }
    alert('You have successfully updated your vehicle!');
  };
});
