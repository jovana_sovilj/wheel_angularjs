(function () {
  'use strict';
  angular
    .module('wheel')
    .controller('SignInController', SignInController);
  function SignInController($scope, $log, $location, $sessionStorage, ref) {
    var users = ref.child('users');
    $scope.user = {};
    $scope.checkAndLoginUser = function (isValid) {
      if (isValid) {
        $log.log($scope.user);
        users.authWithPassword({
          email: $scope.user.email,
          password: $scope.user.password
        }, function (error, authData) {
          if (error) {
            $log.log("Login Failed!", error);

            //$('.login-failed-alert').css({"display": "block"});
            //$('.login-failed-alert p').text(error + ' Please try again.');

          } else {
            var user = users.child(ref.getAuth().uid);
            user.once("value", function (snapshot) {
              var userObj = snapshot.val();
              var activated = snapshot.val().activated;
              if(activated === true) {
                $sessionStorage.loggedInUser = userObj;
                $sessionStorage.registrationFlag = false;
                $sessionStorage.serviceFlag = false;
                $log.log($sessionStorage.loggedInUser);
              }
              var role = user.child('role');
              role.on("value", function (snapshot) {
                if (snapshot.val().toUpperCase() === 'Vehicle owner'.toUpperCase()) {
                  UserVehicleOwner(activated);
                }
                else if (snapshot.val().toUpperCase() === 'Mechanic'.toUpperCase()) {
                  UserMechanic(activated);
                }
                else if (snapshot.val().toUpperCase() === 'Admin'.toUpperCase()) {
                  Admin();
                }
              });
            });
            $log.log("Authenticated successfully with payload:", authData);
          }
        });
      }
      function UserVehicleOwner(active) {
        var activated = active;
        if (activated === true) {
          $location.path("owner");
        }
        else {
          ref.unauth();

          //$('.login-failed-alert').css({"display": "block"});
        }
      }
      function UserMechanic(active) {
        var activated = active;
        if (activated === true) {
          alert('You will be moved to mechanic index later!');
        }
        else {
          ref.unauth();

          //$('.login-failed-alert').css({"display": "block"});
        }
      }
      function Admin() {
        alert('You will be moved to admin index later!');
      }
    };
    $scope.ForgotPassword = function () {
      ref.resetPassword({
        email: $scope.user.email
      }, function (error) {
        if (error) {
          switch (error.code) {
            case "INVALID_USER":
              $log.log("The specified user account does not exist.");
              //$('.login-failed-alert p').text('The specified user account does not exist.');
              //$('.login-failed-alert').css({"display": "block"});
              break;
            default:
              $log.log("Error resetting password:", error);
              //$('.login-failed-alert p').text("Error resetting password: " + error);
              //$('.login-failed-alert').css({"display": "block"});
          }
        } else {
          $log.log("Password reset email sent successfully!");
          //$('.login-failed-alert p').text("Password reset email sent successfully!");
          //$('.login-failed-alert').removeClass('alert-danger').addClass('alert-success').css({"display": "block"});
          //$('.login-failed-alert strong').remove();
        }
      });
    };
  }

})();
