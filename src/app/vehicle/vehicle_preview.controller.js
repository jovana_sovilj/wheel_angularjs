(function () {
  'use strict';
  angular
    .module('wheel')
    .controller('VehiclePreviewController', VehiclePreviewController)
    .controller('CarouselDemoCtrl', CarouselDemoCtrl);
  function VehiclePreviewController($scope, ref, $sessionStorage, VehiclePreview, Carousel, Entries) {
    var users = ref.child('users');
    var vehicles = ref.child('vehicles');
    VehiclePreview.initializeScope($scope);
    $scope.loadMainPhoto = (function () {
      Carousel.showMainPhoto($scope);
    })();
    $scope.loadOtherPhotos = (function () {
      Carousel.showOtherPhotos($scope);
    })();
    (function addBrands() {
      VehiclePreview.edmundsBrands($scope);
    })();
    $scope.selectModelFromEdmunds = function () {
      VehiclePreview.edmundsModels($scope);
    };
    (function showVehicleDetails() {
      VehiclePreview.vehicleDetails($scope, users);
    })();
    $scope.updateVehicle = function () {
      VehiclePreview.vehicleUpdate($scope, vehicles);
    };
    (function showEntriesInTable() {
      Entries.loadEntries($scope, users);
    })();
    $scope.showPreviousServicingModal = function (obj) {
      Entries.openServiceEntryInModal($scope, obj);
    };
    $scope.closePreviousServicingModal = function () {
      $scope.showService = false;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    // mechanic - ONLY MECHANIC CAN CHANGE PREVIOUS SERVICING (KM AND DESCRIPTION)
    $scope.updateServiceEntry = function (entryObj) {
      console.log(entryObj);
    };
    //
    if ($sessionStorage.loggedInUser.role == 'Vehicle owner') {
      $scope.hideSubmitButtonFromOwner = true;
    }
    window.onclick = function (event) {
      if (event.target.className == 'modal') {
        $scope.closePreviousServicingModal();
      }
    };
  }
  function CarouselDemoCtrl($scope, Carousel) {
    (function Pictures() {
      Carousel.pictures($scope);
    })();
  }
})();
