(function () {
  'use strict';
  angular
    .module('wheel')
    .controller('EmailController', EmailController);
  function EmailController($scope, ref, EmailService) {
    var users = ref.child('users');
    var messages = ref.child('messages');
    var messageDrafts = ref.child('messageDrafts');
    EmailService.initializeScope($scope);
    $scope.setTab = function (str) {
      $scope.tabs = str;
    };
    $scope.checkTab = function(obj) {
      EmailService.ifTab(obj, $scope);
    };
    $scope.openMessage = function(obj) {
      EmailService.showMessage(obj, $scope, users);
    };
    $scope.closeMessage = function() {
      $scope.showMessageInModal = false;
      $scope.sendNewMessageModal = false;
    };
    $scope.loadMessages = function () {
      EmailService.loadAllMessagesByTabs($scope, users, messages);
    };
    $scope.sendNewMessage = function(obj) {
      EmailService.openModalToSendMessage(obj, $scope);
    };
    $scope.saveDraft = function() {
      EmailService.saveNewDraft($scope, messageDrafts);
    };
    $scope.deleteDraft = function(key) {
      EmailService.deleteSpecificDraft($scope, key);
    };
    $scope.respondAndSend = function() {
      EmailService.sendNewMessageToOwner($scope, messages);
    };
  }
})();
