(function () {
  'use strict';
  angular
    .module('wheel')
    .controller('ForSaleOwnerController', ForSaleOwnerController);
  function ForSaleOwnerController($scope, $location, ref, ForSaleAndOwner, EmailService) {
    var users = ref.child('users');
    var vehicles = ref.child('vehicles');
    var messages = ref.child('messages');
    var messageDrafts = ref.child('messageDrafts');
    var notifications = ref.child('notifications');
    ForSaleAndOwner.initializeScope($scope);
    var today = new Date();
    var max = today.getFullYear();
    $scope.yearTo = max.toString();
    (function fillYears() {
      var min = 1980;
      for (var i = min; i <= max; i++) {
        $scope.years.push(i);
      }
    })();
    (function AddBrands() {
      $.getJSON("https://api.edmunds.com/api/vehicle/v2/makes?fmt=json&api_key=qhf7nezafzvdbbzum58558qm", function (data) {
        for (var i = 1; i < data.makesCount; i++) {
          $scope.brands.push(data.makes[i].name);
        }
      });
    })();
    $scope.limitYearFrom = function (year) {
      return year <= $scope.yearTo;
    };
    $scope.limitYearTo = function (year) {
      return year >= $scope.yearFrom;
    };
    $scope.limitKmFrom = function (km) {
      return km <= $scope.kmTo;
    };
    $scope.limitKmTo = function (km) {
      return km >= $scope.kmFrom;
    };
    $scope.onlyCarsForSale = function (vehicle) {
      return vehicle.forSale === 'yes';
    };
    $scope.filter = function () {
      if ($location.path().indexOf('sale') != -1) {
        FilterForSale();
      }
      if ($location.path().indexOf('owner') != -1) {
        FilterOwnerIndex();
      }
    };
    function FilterForSale() {
      ForSaleAndOwner.filterVehiclesForSale($scope);
    }
    $scope.setTab = function (str) {
      $scope.tabs = str;
    };
    function FilterOwnerIndex() {
      ForSaleAndOwner.filterVehiclesOnOwnerIndex($scope, vehicles);
    }
    $scope.showAddNewVehicleModal = function () {
      $scope.showModalForAdding = true;
    };
    $scope.closeAddNewVehicleModal = function () {
      $scope.showModalForAdding = false;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.selectModelFromEdmunds = function () {
      ForSaleAndOwner.edmundsModels($scope);
    };
    $scope.AddNewVehicle = function () {
      ForSaleAndOwner.addNewVehicle($scope);
    };
    $scope.sendMessageToOwner = function(id) {
      $scope.sendNewMessageModal = true;
      var userId = users.child(id);
      userId.on("value", function (snapshot) {
        var user = snapshot.val();
        var name = user.name;
        $scope.newMessageTo = name;
        if (!$scope.$$phase) {
          $scope.$apply();
        }
      });
    };
    $scope.closeMessage = function() {
      $scope.sendNewMessageModal = false;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.respondAndSend = function() {
      EmailService.sendNewMessageToOwner($scope, messages);
    };
    $scope.saveDraft = function() {
      EmailService.saveNewDraft($scope, messageDrafts);
    };
    $scope.openSellVehicleModal = function(vehicleId) {
      $scope.userId = '';
      $scope.showModalSell = true;
      $scope.vehicleId = vehicleId;
    };
    $scope.closeModalSell = function() {
      $scope.showModalSell = false;
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.sellVehicleToAnotherOwner = function() {
      ForSaleAndOwner.sendNotificationToNewOwner($scope, users, notifications);
    };
    window.onclick = function (event) {
      if (event.target.className == 'modal') {
        $scope.closeMessage();
        $scope.closeModalSell();
        $scope.closeAddNewVehicleModal();
      }
    };
  }
})();
