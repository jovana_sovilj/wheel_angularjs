(function () {
  'use strict';

  angular
    .module('wheel', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngRoute', 'ui.bootstrap', 'toastr', 'ngStorage']);

  angular.module('wheel').directive("carForSale", function () {
    return {
      restrict: 'AECM',
      templateUrl: 'app/templates_for_directives/car_for_sale.html',
      replace: true,
      scope: {
        carObject: '=',
        ref: '=',
        sendMessage: '=',
        sellVehicle: '='
      }
    }
  });
  angular.module('wheel').directive("notification", function () {
    return {
      restrict: 'AECM',
      templateUrl: 'app/templates_for_directives/new_notification.html',
      replace: true,
      scope: {
        notificationObject: '=',
        declineSell: '=',
        okOnResponse: '=',
        acceptSell: '='
      }
    }
  });
  angular.module('wheel').directive("customHeader", function () {
    return {
      restrict: 'AECM',
      templateUrl: 'app/templates_for_directives/header.html',
      replace: true
    }
  });
  angular.module('wheel').directive("addNewPhotoModal", function () {
    return {
      restrict: 'AECM',
      templateUrl: 'app/modals/add_new_photo.html',
      replace: true
    }
  });
  angular.module('wheel').directive("previousServicingModal", function () {
    return {
      restrict: 'AECM',
      templateUrl: 'app/modals/previous_service_modal.html',
      replace: true
    }
  });
  angular.module('wheel').directive("addNewVehicleModal", function () {
    return {
      restrict: 'AECM',
      templateUrl: 'app/modals/add_new_vehicle.html',
      replace: true
    }
  });
  angular.module('wheel').directive("messageModal", function () {
    return {
      restrict: 'AECM',
      templateUrl: 'app/modals/message_preview.html',
      replace: true
    }
  });
  angular.module('wheel').directive("newMessageModal", function () {
    return {
      restrict: 'AECM',
      templateUrl: 'app/modals/new_message.html',
      replace: true
    }
  });
  angular.module('wheel').directive("sellVehicleModal", function () {
    return {
      restrict: 'AECM',
      templateUrl: 'app/modals/sell_modal.html',
      replace: true
    }
  });


})();
