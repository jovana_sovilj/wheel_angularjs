(function () {
  'use strict';
  angular
    .module('wheel')
    .controller('SignUpController', SignUpController);
  function SignUpController($scope, $log, ref) {
    $scope.user = {};
    $scope.success = true;
    $scope.usertypes = [
      {userID: 'Legal entity', userType: 'Legal entity'},
      {userID: 'Individual', userType: 'Individual'}
    ];
    $scope.userroles = [
      {roleID: 'Vehicle owner', userRole: 'Vehicle owner'},
      {roleID: 'Mechanic', userRole: 'Mechanic'}
    ];
    $scope.checkAndSaveUser = function (isValid) {
      if (isValid) {
        var id = $scope.user.userId.toString();
        ref.createUser({
          userName: $scope.user.userName,
          password: $scope.user.password,
          email: $scope.user.email,
          legalPersonality: $scope.user.userType,
          name: $scope.user.name,
          role: $scope.user.iAm,
          id: id
        }, function (error, userData) {
          if (error) {
            switch (error.code) {
              case "EMAIL_TAKEN":
                return $log.log("The new user account cannot be created because the email is already in use.");
                break;
              case "INVALID_EMAIL":
                return $log.log("The specified email is not a valid email.");
                break;
              default:
                return $log.log("Error creating user:", error)
            }
          } else {
            setMeToFalse();
            ref.child("users").child(userData.uid).set({
              "userName": $scope.user.userName,
              "email": $scope.user.email,
              "legalPersonality": $scope.user.userType,
              "name": $scope.user.name,
              "role": $scope.user.iAm,
              "id": id,
              "activated": false
            });
            return $log.log("Successfully created user account with uid:", userData.uid);
          }
        });
      }
    };
    function setMeToFalse() {
      $scope.success = false;
    }
  }
})();
