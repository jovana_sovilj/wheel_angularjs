(function () {
  'use strict';
  angular
    .module('wheel')
    .controller('ProfileController', ProfileController);
  angular
    .module('wheel').directive('focusInput', function ($timeout) {
    return {
      link: function (scope, element) {
        element.bind('click', function () {
          $timeout(function () {
            element.parent().parent().find('input')[0].focus();
          });
        });
      }
    };
  });
  function ProfileController($scope, ref, $sessionStorage, $location, profileUpdate) {
    var users = ref.child('users');
    var vehicles = ref.child('vehicles');
    var messages = ref.child('messages');
    var notifications = ref.child('notifications');
    if (ref.getAuth() != null || ref.getAuth() != undefined) {
      var userId = users.child(ref.getAuth().uid);
      if (userId != null || userId != undefined) {
        if ($sessionStorage.loggedInUser != null || $sessionStorage.loggedInUser != undefined) {
          profileUpdate.fillInputsOnProfile($scope);
          $scope.profileUpdate = function () {
            var uName = $scope.userName;
            var name = $scope.name;
            var id = $scope.id;
            var type = $scope.userType;
            var role = $scope.iAm;
            var oldPass = $scope.oldPassword;
            var newPass = $scope.newPassword;
            var email = $scope.email;
            var passEmail = $scope.pass;
            profileUpdate.updateUserData(id, type, name, role, uName, userId, $scope);
            if (oldPass != undefined && newPass != undefined) {
              updatePassword(oldPass, newPass);
            }
            if (email != undefined && passEmail != undefined) {
              updateEmail(email, passEmail);
            }
            function updateEmail(newEmail, pass) {
              profileUpdate.emailUpdate(newEmail, pass);
            }
            function updatePassword(oldOne, newOne) {
              profileUpdate.passwordUpdate(oldOne, newOne, userId);
            }
          };
          $scope.myCarsArr = [];
          ref.child('vehicles').orderByChild("ownerId").equalTo(ref.getAuth().uid).once("value",
            function (snapshot) {
              snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                $scope.myCarsArr.push(childData);
              });
            });
          $scope.showThisCar = function (id) {
            $sessionStorage.vehicleId = id;
            $location.path("vehicle");
          };
        }
        else {
          alert('Your session has expired, please sign out, and sign in again.')
        }
      }
    }
  }
})();
