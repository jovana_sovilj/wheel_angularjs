(function () {
  'use strict';
  angular
    .module('wheel')
    .controller('LoadHeaderController', LoadHeaderController);
  function LoadHeaderController($scope, $location, ref, HeaderManipulation) {
    var users = ref.child('users');
    var notifications = ref.child('notifications');
    var vehicles = ref.child('vehicles');
    $scope.logged = null;
    $scope.role = null;
    $scope.notificationsArr = [];
    $scope.reminder = false;
    HeaderManipulation.loadDifferentHeader($scope, users);
    $scope.signOut = function () {
      ref.unauth();
      $location.path("");
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };
    $scope.unreadMessages = function () {
      HeaderManipulation.showUnreadMessages($scope, users);
    };
    $scope.unreadMessages();
    $scope.unseenNotifications = function () {
      HeaderManipulation.showUnseenNotifications($scope);
    };
    $scope.unseenNotifications();
    $scope.registrationReminder = function () {
      HeaderManipulation.registration($scope, notifications);
    };
    $scope.registrationReminder();
    $scope.serviceReminder = function () {
      HeaderManipulation.service($scope, notifications);
    };
    $scope.serviceReminder();
    $scope.declineNotificationSell = function (key) {
      HeaderManipulation.declineSellInNotification(key, $scope, users, notifications);
    };
    $scope.okResponseNotification = function (key) {
      HeaderManipulation.okResponseOnNotification(key, $scope, notifications);
    };
    $scope.acceptNotificationSell = function (key) {
      HeaderManipulation.acceptSellInNotification(key, $scope, notifications, users, vehicles);
    };

  }
})();
