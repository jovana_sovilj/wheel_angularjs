(function() {
  'use strict';

  angular
    .module('wheel')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/for-sale', {
        templateUrl: 'app/sale/for_sale.html',
        controller: 'ForSaleOwnerController'
      })
      .when('/profile', {
        templateUrl: 'app/profile/profile.html',
        controller: 'ProfileController'
      })
      .when('/vehicle', {
        templateUrl: 'app/vehicle/vehicle_preview.html',
        controller: 'VehiclePreviewController'
      })
      .when('/mail', {
        templateUrl: 'app/email/email_preview.html',
        controller: 'EmailController'
      })
      .when('/owner', {
        templateUrl: 'app/owner/owner_index.html',
        controller: 'ForSaleOwnerController'
      })
      .when('/about', {
        templateUrl: 'app/about/about.html'
      })
      .when('/sign-in', {
        templateUrl: 'app/sign_in/sign_in.html',
        controller: 'SignInController'
      })
      .when('/sign-up', {
        templateUrl: 'app/sign_up/sign_up.html',
        controller: 'SignUpController'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
