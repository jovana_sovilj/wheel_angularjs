/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('wheel')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('ref', new Firebase("https://wheelapp.firebaseio.com"));

})();
